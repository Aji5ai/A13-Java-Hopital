import java.util.ArrayList;

public class Hospital {
    public static void main(String[] args) {
        // Création médicaments
        Medication med1 = new Medication("Med1", "10mg");
        Medication med2 = new Medication("Med2", "20mg");
        Medication med3 = new Medication("Med3", "25mg");
        Medication med4 = new Medication("Med4", "5mg");
        
        // Création listes de médicaments
        ArrayList<Medication> medicationList1 = new ArrayList<>();
        ArrayList<Medication> medicationList2 = new ArrayList<>();
        medicationList1.add(med1);
        medicationList1.add(med2);
        medicationList2.add(med3);
        medicationList2.add(med4);
        
        // Création maladies avec listes de médicaments
        Illness illness1 = new Illness("Illness1", medicationList1);
        Illness illness2 = new Illness("Illness2", medicationList2);
        Illness illness3 = new Illness("Illness3", medicationList2);
        Illness illness4 = new Illness("Illness4", medicationList1);
        
        // Création patients
        ArrayList<Illness> patientIllnessList1 = new ArrayList<>();
        ArrayList<Illness> patientIllnessList2 = new ArrayList<>();
        patientIllnessList1.add(illness1);
        patientIllnessList1.add(illness2);
        Patient patient1 = new Patient("John Doe", 30, "123-456-789", "P001", patientIllnessList1);
        patientIllnessList2.add(illness3);
        patientIllnessList2.add(illness4);
        Patient patient2 = new Patient("David Bébert", 56, "722-466-589", "P002", patientIllnessList2);
        
        // Création docteurs
        Doctor doctor1 = new Doctor("D001", "Dr. Smith", 40, "987-654-321", "Cardiology");
        Doctor doctor2 = new Doctor("D002", "Dr. Docteur", 18, "907-154-311", "Hematology");
        
        // Création nurses
        Nurse nurse1 = new Nurse("N001", "Ema Johnson", 35, "654-321-587");
        Nurse nurse2 = new Nurse("N002", "Charlotte Bibi", 42, "154-421-983");
        
        // Affichage des informations
        System.out.println("Patient1 Information:");
        System.out.println(patient1.getInfo());
        System.out.println("Patient2 Information:");
        System.out.println(patient2.getInfo());
        
        System.out.println("\nDoctor1 Information:");
        doctor1.getRole();
        System.out.println("name : " + doctor1.name + " \nage : " + doctor1.age + " \nId : " + doctor1.employeeId);
        System.out.println("Speciality: " + doctor1.getSpeciality());
        System.out.println("\nDoctor2 Information:");
        doctor2.getRole();
        System.out.println("name : " + doctor2.name + " \nage : " + doctor2.age + " \nId : " + doctor2.employeeId);
        System.out.println("Speciality: " + doctor2.getSpeciality());
        
        System.out.println("\nNurse1 Infos:");
        System.out.println("name : " + nurse1.name + " \nage : " + nurse1.age + " \nId : " + nurse1.employeeId);
        System.out.println("\nNurse1 Role:");
        nurse1.getRole();
        System.out.println("\nNurse2 Infos:");
        System.out.println("name : " + nurse2.name + " \nage : " + nurse2.age + " \nId : " + nurse2.employeeId);
        System.out.println("\nNurse2 Role:");
        nurse2.getRole();

        System.out.println("\n" + med1.getInfo());
        nurse1.careForPatient(patient2);

        doctor2.recordPatientVisit("Va mourir dans 2 jours.");
    }
}

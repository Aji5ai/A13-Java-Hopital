public interface Care {
    // par défault les méthodes sont public
    void careForPatient(Patient patient);
    
    // default necessaire pour une interface si on donne une implémentation par défaut dans une méthode
    // les classes qui implémentent Care pourront soit garder cette méthode par défaut, soit l'override
    default void recordPatientVisit(String notes) {
        System.out.println("Patient visit recorded: " + notes);
    }
}

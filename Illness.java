import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList;

    public Illness (String name, ArrayList<Medication> medicationList) {
        this.name = name;
        this.medicationList = new ArrayList<>(); // "Si vous avez plusieurs objets Illness et que chacun d'eux doit gérer sa propre liste de médicaments, cette approche est nécessaire pour éviter que les changements dans une liste n'affectent les autres."
    }

    public void addMedication(Medication medication){
        medicationList.add(medication);
    }

    public String getInfo(){
        return name + " " + medicationList;
    }
}

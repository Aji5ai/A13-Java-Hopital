import java.util.ArrayList;

public class Patient extends Person{
    private String patientId;
    private ArrayList<Illness> illnessList;

    public Patient(String name, int age, String socialSecurityNumber, String patientId, ArrayList<Illness> illnessList){
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void addIllness(Illness illness){
        illnessList.add(illness);
    }

    public String getInfo(){
        return (
            "nom : " + name +
            ", age : " + age +
            ", ID du patient : " + patientId +
            ", num sécu sociale : " + socialSecurityNumber
        );
    }
    // override quand on veut redéfinir une méthode du parent, donc pas besoin ici
}


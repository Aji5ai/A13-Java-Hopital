public class Doctor extends MedicalStaff{
    private String speciality;
    
    public Doctor(String employeeId, String name, int age, String socialSecurityNumber, String speciality) {
        super(employeeId, name, age, socialSecurityNumber);
        this.speciality = speciality;
    }

    public String getSpeciality(){
        return this.speciality;
    }

    @Override
    void getRole() {
        System.out.println("Doctor");
    }
    
    @Override
    public void careForPatient(Patient patient) {
       System.out.println("Doctor " + name + " cares for " + patient.name);
    }
}

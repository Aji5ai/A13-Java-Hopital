public class Nurse extends MedicalStaff{

    public Nurse(String employeeId, String name, int age, String socialSecurityNumber) {
        super(employeeId, name, age, socialSecurityNumber);
    }

    @Override
    void getRole() {
        System.out.println("Nurse");;
    }
    
    @Override
    public void careForPatient(Patient patient) {
       System.out.println("Nurse " + name + " cares for " + patient.name);
    }
}

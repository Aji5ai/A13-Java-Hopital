public abstract class MedicalStaff extends Person implements Care{
    protected String employeeId;

    public MedicalStaff (String employeeId, String name, int age, String socialSecurityNumber){
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    abstract void getRole();
}

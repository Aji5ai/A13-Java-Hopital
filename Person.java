//abstract =  ne peut pas être instanciée directement, sert de modèle pour d'autres classes qui étendent Person et qui implémentent les méthodes abstraites
// peut contenir à la fois des méthodes abstraites (méthodes sans implémentation) et des méthodes concrètes (avec une implémentation).
// au final pas besoin de méthodes abstraites
// avantage abstract : pour empecher de créer uen instance de Person
public abstract class Person {
    // protected = accessibles à toutes les classes dans le même package, ainsi qu'à toutes les sous-classes (classes filles)
    protected String name;
    protected int age;
    protected String socialSecurityNumber;

    public Person (String name, int age, String socialSecurityNumber){
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public String getName(){
        return name;
    };
    public int getAge(){
        return age;
    };
    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    };
}
